FROM tiangolo/uvicorn-gunicorn-fastapi:python3.7

COPY . /python-api

WORKDIR /python-api
RUN ls
RUN pip install --upgrade pip
RUN ls
RUN apt-get update
RUN apt-get install -y locales locales-all
RUN update-locale
RUN pip install -r ./back/requirements.txt
CMD ["uvicorn", "back.main:app", "--host", "0.0.0.0", "--port", "40000"]
