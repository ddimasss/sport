from datetime import datetime, time, date
from typing import Optional
from .luna import LunaBase


class Program(LunaBase):
    class Config:
        table = 'programs'
        relation = [{'table': 'coaches', 'field_in': 'coach_id', 'field_out': 'id', 'except': ['disabled']}]
    id: Optional[int]
    start_date: Optional[date] = datetime.now()
    name: str
    short_name: str
    description: Optional[str] = ''
    member_level: str
    coach_id: int
    monday_time: Optional[time]
    monday_interval: Optional[int]
    monday_time2: Optional[time]
    monday_interval2: Optional[int]
    tuesday_time: Optional[time]
    tuesday_interval: Optional[int]
    tuesday_time2: Optional[time]
    tuesday_interval2: Optional[int]
    wednesday_time: Optional[time]
    wednesday_interval: Optional[int]
    wednesday_time2: Optional[time]
    wednesday_interval2: Optional[int]
    thursday_time: Optional[time]
    thursday_interval: Optional[int]
    thursday_time2: Optional[time]
    thursday_interval2: Optional[int]
    friday_time: Optional[time]
    friday_interval: Optional[int]
    friday_time2: Optional[time]
    friday_interval2: Optional[int]
    saturday_time: Optional[time]
    saturday_interval: Optional[int]
    saturday_time2: Optional[time]
    saturday_interval2: Optional[int]
    sunday_time: Optional[time]
    sunday_interval: Optional[int]
    sunday_time2: Optional[time]
    sunday_interval2: Optional[int]
    price_in_cents: int
    gym_id: int
    image: Optional[str] = []
    disabled: Optional[int] = None
    visit_count: int
