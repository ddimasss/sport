from datetime import datetime, date
from typing import Optional
from .luna import LunaBase


class Member2Program(LunaBase):
    class Config:
        table = 'members_to_programs'
        relation = [
            {'table': 'members', 'field_in': 'id_member', 'field_out': 'id', 'except': ['disabled']},
            {'table': 'programs', 'field_in': 'id_program', 'field_out': 'id', 'except': ['disabled']}
        ]

    id_member: int
    id_program: int
    joining_date: Optional[date] = datetime.now()
    price_period: Optional[int] = None
    price_workout_count: Optional[int] = None
    price_in_cents: Optional[int] = None
    disabled: Optional[int] = None
    period_type: Optional[str] = ''
