from datetime import datetime
from typing import Optional
from .luna import LunaBase


class User(LunaBase):
    class Config:
        table = 'users'
    id: Optional[int]
    created_at: Optional[datetime] = datetime.now()
    fio: Optional[str] = ''
    mobile: str
    mobile2: Optional[str] = ''
    username: str
    password: str
    invite_code: Optional[str] = ''
    group_id: Optional[int] = 0
    who: Optional[str] = 'system'

