from datetime import datetime
from typing import Optional
from .luna import LunaBase


class Changes(LunaBase):
    class Config:
        table = 'changes'
    id: Optional[int]
    date_time: Optional[datetime] = datetime.now()
    program_id: int
    coach_id: int
    reason: str
    who: str
