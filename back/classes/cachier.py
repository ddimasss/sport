from datetime import datetime, date
from typing import Optional
from .luna import LunaBase


class Cashier(LunaBase):
    class Config:
        table = 'cashier'
        relation = [
            {'table': 'users', 'field_in': 'user_id', 'field_out': 'id', 'except': ['disabled']}
        ]
    id: Optional[int]
    user_id: int
    date_time: Optional[datetime] = datetime.now()
    operation: str
    amount_in_cents: int
    who: str
    description: Optional[str] = ''
    disabled: Optional[int] = 0
