from pydantic import BaseModel
import psycopg2
from psycopg2.extensions import AsIs
from psycopg2.extras import DictCursor, RealDictCursor
from back.db_config import postgres_ip, postgres_port, user, password, db_name


class LunaBase(BaseModel):
    def save(self):
        dict_ = self.dict()
        print(dict_)
        if not hasattr(self, 'id') or self.id is None:
            if hasattr(self, 'id'):
                del(dict_['id'])
            print(dict_)
            columns = list(dict_.keys())
            values = [dict_[column] for column in columns]
            insert_statement = f'''insert into {self.Config.table} (%s) values %s RETURNING *'''
            print(insert_statement)
            with psycopg2.connect(dbname=db_name, user=user, password=password, host=postgres_ip, port=postgres_port) as conn:
                with conn.cursor() as cursor:
                    cursor.execute(insert_statement, (AsIs(','.join(columns)), tuple(values)))
                    res = cursor.fetchall()
                    self = res[0]
        else:
            columns = list(dict_.keys())
            values = [dict_[column] for column in columns]
            update_statement = f'''update {self.Config.table} set (%s) = %s where id = {self.id};'''
            print(update_statement)
            with psycopg2.connect(dbname=db_name, user=user, password=password, host=postgres_ip, port=postgres_port) as conn:
                with conn.cursor() as cursor:
                    cursor.execute(update_statement, (AsIs(','.join(columns)), tuple(values)))


def get_objects(table, relation=[], filter='1=1', sort='id', limit='ALL', offset=0):
    join = ''
    sort = table+'.'+sort
    for rel in relation:
        join += f'''left join {rel['table']} on ({table}.{rel['field_in']} = {rel['table']}.{rel['field_out']}) '''
    if table == 'bill':
        join += f'''left join (select bill_id, COALESCE(count(*),0) as total_visits from visits group by bill_id) v1 on (bill.id = v1.bill_id)'''
    print(f'select * from {table} {join} where {filter} order by {sort} limit {limit} offset {offset};')
    with psycopg2.connect(dbname=db_name, user=user, password=password, host=postgres_ip, port=postgres_port) as conn:
        with conn.cursor(cursor_factory=DictCursor) as cursor:
            cursor.execute(
                f'select * from {table} {join} where {filter} order by {sort} limit {limit} offset {offset};'
            )
            res = []
            if cursor.rowcount > 0:
                res = []
                colnames = [desc[0] for desc in cursor.description]
                for row in cursor:
                    rowdict = {}
                    for x in range(0, len(row)):
                        # make sure column name is unique, if not append index number
                        if colnames[x] in rowdict:
                            rowdict[colnames[x] + "_" + str(x)] = row[x]
                        else:
                            rowdict[colnames[x]] = row[x]
                    res.append(rowdict)
            return res

def get_objects_by_sql(sql: str) -> dict:
    with psycopg2.connect(dbname=db_name, user=user, password=password, host=postgres_ip, port=postgres_port) as conn:
        with conn.cursor(cursor_factory=DictCursor) as cursor:
            cursor.execute(sql)
            return cursor.fetchall()

