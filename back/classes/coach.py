from datetime import datetime
from typing import Optional
from .luna import LunaBase


class Coach(LunaBase):
    class Config:
        table = 'coaches'
    id: Optional[int]
    fio: str
    mobile: str
    mobile2: Optional[str] = None
    description: Optional[str] = None
    username: str
    password: Optional[str] = None
    invite_code: Optional[str] = None
    position: Optional[str] = None
    disabled: Optional[int] = None
    image: Optional[str] = 'assets/images/rtm.jpg'

