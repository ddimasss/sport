from datetime import datetime, date
from typing import Optional
from .luna import LunaBase


class Member(LunaBase):
    class Config:
        table = 'members'
        relation = [{'table': 'members_to_programs', 'field_in': 'id', 'field_out': 'id_member', 'except': ['disabled']}]
    id: Optional[int]
    member_card: Optional[int] = None
    locker_number: Optional[int] = None
    fio: str
    birthday: date
    gender: int
    mobile: str
    mobile2: Optional[str] = ''
    description: Optional[str] = ''
    joining_date: Optional[date] = datetime.now()
    parent_name: Optional[str] = ''
    parent_mobile: Optional[str] = ''
    parent_mobile2: Optional[str] = ''
    username: str
    password: str
    invite_code: Optional[str] = ''
    disabled: Optional[int] = None
