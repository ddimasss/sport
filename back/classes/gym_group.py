from typing import Optional
from .luna import LunaBase


class GymGroup(LunaBase):
    class Config:
        table = 'gym_group'
    id: Optional[int]
    name: str
    description: Optional[str] = ''
    people_count: int
    address: str

