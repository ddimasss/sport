from datetime import datetime, date
from typing import Optional
from .luna import LunaBase


class Bill(LunaBase):
    class Config:
        table = 'bill'
        relation = [
            {'table': 'members', 'field_in': 'member_id', 'field_out': 'id', 'except': ['disabled']},
            {'table': 'programs', 'field_in': 'program_id', 'field_out': 'id', 'except': ['disabled']}
        ]
    id: Optional[int]
    date_time: Optional[datetime] = datetime.now()
    program_id: int
    member_id: int
    amount_in_cents: int
    who: str
    start_date: Optional[date] = datetime.now()
    end_date: Optional[date] = None
    visits_count: Optional[int] = None
    delay_in_cents: Optional[int] = None
