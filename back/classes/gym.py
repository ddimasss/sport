from datetime import time
from typing import Optional
from .luna import LunaBase


class Gym(LunaBase):
    class Config:
        table = 'gym'
    id: Optional[int]
    name: str
    description: Optional[str] = ''
    people_count: int
    open_time: time
    close_time: time
    gorup_id: int
