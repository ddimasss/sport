from datetime import datetime
from typing import Optional
from .luna import LunaBase


class Visit(LunaBase):
    class Config:
        table = 'visits'
    id: Optional[int]
    date_time: Optional[datetime] = datetime.now()
    bill_id: int
    member_id: int
    locker_number: Optional[int] = -1
    who: Optional[str] = "system"
    disabled: Optional[int] = 0
