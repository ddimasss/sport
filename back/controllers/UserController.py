from back.classes.user import User
from back.classes.luna import get_objects


def get_action(filter='1=1', sort='id', limit='ALL', offset=0):
    res = get_objects('users', filter=filter, sort=sort, limit=limit, offset=offset)
    return res


def post_action(user: User):
    user.save()
    return user.dict()
