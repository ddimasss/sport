from back.classes.visit import Visit
from back.classes.luna import get_objects


def get_action(filter='1=1', sort='id', limit='ALL', offset=0):
    res = get_objects('visits', filter=filter, sort=sort, limit=limit, offset=offset)
    return res


def post_action(cls: Visit):
    cls.save()
    return cls.dict()
