from back.classes.gym_group import GymGroup
from back.classes.luna import get_objects


def get_action(filter='1=1', sort='id', limit='ALL', offset=0):
    res = get_objects('gym_group', filter=filter, sort=sort, limit=limit, offset=offset)
    return res


def post_action(cls: GymGroup):
    cls.save()
    return cls.dict()
