from back.classes.bill import Bill as CLS
from back.classes.luna import get_objects


def get_action(filter='1=1', sort='id', limit='ALL', offset=0):
    res = get_objects(CLS.Config.table, CLS.Config.relation, filter=filter, sort=sort, limit=limit, offset=offset)
    return res


def post_action(cls: CLS):
    cls.save()
    return cls.dict()
