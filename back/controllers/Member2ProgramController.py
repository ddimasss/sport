from back.classes.member2program import Member2Program as CLS
from back.classes.luna import get_objects
import psycopg2
from psycopg2.extensions import AsIs
from psycopg2.extras import DictCursor, RealDictCursor
from back.db_config import postgres_ip, postgres_port, user, password, db_name


def get_action(filter='1=1', sort='id_member', limit='ALL', offset=0):
    res = get_objects(CLS.Config.table, CLS.Config.relation, filter=filter, sort=sort, limit=limit, offset=offset)
    return res


def post_action(cls: CLS):
    cls.save()
    return cls.dict()

def delete_action(cls: CLS):
    dict_ = cls.dict()
    columns = list(dict_.keys())
    values = [dict_[column] for column in columns]
    update_statement = f'''delete from {cls.Config.table}
                           where id_member = {cls.id_member} 
                                 and id_program =  {cls.id_program};'''
    print(update_statement)
    with psycopg2.connect(dbname=db_name, user=user, password=password, host=postgres_ip, port=postgres_port) as conn:
        with conn.cursor() as cursor:
            cursor.execute(update_statement, (AsIs(','.join(columns)), tuple(values)))
