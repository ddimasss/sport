from back.classes.coach import Coach
from back.classes.luna import get_objects, get_objects_by_sql


def get_action(filter='1=1', sort='id', limit='ALL', offset=0):
    res = get_objects('coaches', filter=filter, sort=sort, limit=limit, offset=offset)
    return res


def post_action(cls: Coach):
    cls.save()
    return cls.dict()

def get_salary(month: str = None, coach_fio: str = None):
    """ Month in 2020-02-01 format """
    filter: str = ''
    if month is None and coach_fio is None:
        filter = ' and 1=1 '
    else:
        if month is not None:
            filter = f" and date_trunc('month', b.date_time) = '{month}' "
        if coach_fio is not None:
            filter += f" and c.fio = '{coach_fio}' "
    sql = f'''
        select b.id, b.date_time, b.end_date, count(v.id) as visits_count, b.visits_count as visits_max, b.amount_in_cents / 100 as amount, p."name", c.fio as coach_fio, m.fio as member_fio from bill b 
        left join visits v on (v.bill_id  = b.id and (v.disabled is null or v.disabled = 0))
        left join programs p on (b.program_id = p.id)
        left join coaches c on (p.coach_id = c.id )
        left join members m on (b.member_id = m.id)
        where (b.disabled is null or b.disabled = 0)
         {filter}
        group by b.id, b.date_time, b.end_date, b.amount_in_cents, b.visits_count, p."name", c.fio, m.fio 
        order by b.date_time desc
    '''
    return get_objects_by_sql(sql);
