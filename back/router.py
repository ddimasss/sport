from fastapi import APIRouter
from back.controllers import UserController, MemberController, BillController, ChangesController, CoachController, \
                             GymGroupController, GymController, ProgramController, VisitController, \
                             Member2ProgramController, CashierController
from back.classes.user import User
from back.classes.member import Member
from back.classes.bill import Bill
from back.classes.changes import Changes
from back.classes.coach import Coach
from back.classes.gym_group import GymGroup
from back.classes.gym import Gym
from back.classes.program import Program
from back.classes.visit import Visit
from back.classes.member2program import Member2Program
from back.classes.cachier import Cashier

router = APIRouter()

@router.get("/users")
async def read_items(filter='1=1', sort='id', limit='ALL', offset = 0):
    return UserController.get_action(filter=filter, sort=sort, limit=limit, offset = offset)


@router.post("/users/add_update")
def read_items(user: User):
    return UserController.post_action(user)


@router.get("/members")
async def read_items(filter='1=1', sort='id', limit='ALL', offset=0):
    return MemberController.get_action(filter=filter, sort=sort, limit=limit, offset=offset)


@router.post("/members/add_update")
def read_items(cls: Member):
    return MemberController.post_action(cls)


@router.get("/bill")
async def read_items(filter='1=1', sort='id', limit='ALL', offset=0):
    return BillController.get_action(filter=filter, sort=sort, limit=limit, offset=offset)


@router.post("/bill/add_update")
def read_items(cls: Bill):
    return BillController.post_action(cls)


@router.get("/coach")
async def read_items(filter='1=1', sort='id', limit='ALL', offset=0):
    return CoachController.get_action(filter=filter, sort=sort, limit=limit, offset=offset)


@router.post("/coach/add_update")
def read_items(cls: Coach):
    return CoachController.post_action(cls)


@router.get("/coach_salary")
async def read_items(month: str = None, coach_fio: str = None):
    return CoachController.get_salary(month, coach_fio)


@router.get("/gym_group")
async def read_items(filter='1=1', sort='id', limit='ALL', offset=0):
    return GymGroupController.get_action(filter=filter, sort=sort, limit=limit, offset=offset)


@router.post("/gym_group/add_update")
def read_items(cls: GymGroup):
    return GymGroupController.post_action(cls)


@router.get("/gym")
async def read_items(filter='1=1', sort='id', limit='ALL', offset=0):
    return GymController.get_action(filter=filter, sort=sort, limit=limit, offset=offset)


@router.post("/gym/add_update")
def read_items(cls: Gym):
    return GymController.post_action(cls)


@router.get("/program")
async def read_items(filter='1=1', sort='id', limit='ALL', offset=0):
    return ProgramController.get_action(filter=filter, sort=sort, limit=limit, offset=offset)


@router.post("/program/add_update")
def read_items(cls: Program):
    return ProgramController.post_action(cls)


@router.get("/visit/{id}")
async def read_items(id: str, filter='1=1', sort='id', limit='ALL', offset=0):
    return VisitController.get_action(filter='member_id = '+ id +' and ' + filter, sort=sort, limit=limit, offset=offset)


@router.post("/visit/add_update")
def read_items(cls: Visit):
    return VisitController.post_action(cls)


@router.get("/change")
async def read_items(filter='1=1', sort='id', limit='ALL', offset=0):
    return ChangesController.get_action(filter=filter, sort=sort, limit=limit, offset=offset)


@router.post("/change/add_update")
def read_items(cls: Changes):
    return ChangesController.post_action(cls)


@router.get("/members2programs")
async def read_items(filter='1=1', sort='id_member', limit='ALL', offset=0):
    return Member2ProgramController.get_action(filter=filter, sort=sort, limit=limit, offset=offset)


@router.post("/members2programs/add_update")
def read_items(cls: Member2Program):
    return Member2ProgramController.post_action(cls)


@router.post("/members2programs/delete")
def delete_member_program_relation(cls: Member2Program):
    return Member2ProgramController.delete_action(cls)


@router.get("/cashier")
async def read_items(filter='1=1', sort='id', limit='ALL', offset=0):
    return CashierController.get_action(filter=filter, sort=sort, limit=limit, offset=offset)


@router.post("/cashier/add_update")
def read_items(cls: Cashier):
    return CashierController.post_action(cls)
