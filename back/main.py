from fastapi import FastAPI
import back.router as router
from starlette.middleware.cors import CORSMiddleware

app = FastAPI()
app.add_middleware(CORSMiddleware, allow_origins=['*'], allow_methods=["*"],
                   allow_credentials=True, allow_headers=["*"])
app.include_router(router.router)

