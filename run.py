import uvicorn
import os

number_of_cores = 1

if __name__ == "__main__":
    uvicorn.run("back.main:app", host="0.0.0.0", port=40000, log_level="info", workers=number_of_cores)