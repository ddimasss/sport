CREATE TABLE public.bill (
    id integer NOT NULL,
    date_time timestamp without time zone NOT NULL,
    program_id integer NOT NULL,
    member_id integer NOT NULL,
    amount_in_cents integer NOT NULL,
    who character varying(256) NOT NULL,
    start_date timestamp without time zone NOT NULL,
    end_date timestamp without time zone,
    visits_count integer
);


--
-- TOC entry 213 (class 1259 OID 16677)
-- Name: bill_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bill_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2937 (class 0 OID 0)
-- Dependencies: 213
-- Name: bill_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bill_id_seq OWNED BY public.bill.id;


--
-- TOC entry 218 (class 1259 OID 16715)
-- Name: changes; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.changes (
    id integer NOT NULL,
    date_time timestamp without time zone NOT NULL,
    program_id integer NOT NULL,
    coach_id integer NOT NULL,
    reason character varying(256) NOT NULL,
    who character varying(256) NOT NULL
);


--
-- TOC entry 217 (class 1259 OID 16713)
-- Name: changes_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.changes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2938 (class 0 OID 0)
-- Dependencies: 217
-- Name: changes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.changes_id_seq OWNED BY public.changes.id;


--
-- TOC entry 207 (class 1259 OID 16622)
-- Name: coaches; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.coaches (
    id smallint NOT NULL,
    fio character varying(255) NOT NULL,
    mobile character varying(255) NOT NULL,
    mobile2 character varying(255),
    description text,
    username character varying(255) NOT NULL,
    password character varying(255),
    invite_code character varying(255)
);


--
-- TOC entry 206 (class 1259 OID 16620)
-- Name: coaches_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.coaches_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2939 (class 0 OID 0)
-- Dependencies: 206
-- Name: coaches_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.coaches_id_seq OWNED BY public.coaches.id;


--
-- TOC entry 203 (class 1259 OID 16586)
-- Name: gym; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.gym (
    id smallint NOT NULL,
    name character varying(255) NOT NULL,
    description text,
    people_count bigint NOT NULL,
    open_time time without time zone NOT NULL,
    close_time time without time zone NOT NULL,
    gorup_id integer NOT NULL
);


--
-- TOC entry 205 (class 1259 OID 16606)
-- Name: gym_group; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.gym_group (
    id smallint NOT NULL,
    name character varying(255) NOT NULL,
    description text,
    people_count bigint NOT NULL,
    address character varying(255) NOT NULL
);


--
-- TOC entry 204 (class 1259 OID 16604)
-- Name: gym_group_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.gym_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2940 (class 0 OID 0)
-- Dependencies: 204
-- Name: gym_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.gym_group_id_seq OWNED BY public.gym_group.id;


--
-- TOC entry 202 (class 1259 OID 16584)
-- Name: gym_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.gym_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2941 (class 0 OID 0)
-- Dependencies: 202
-- Name: gym_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.gym_id_seq OWNED BY public.gym.id;


--
-- TOC entry 211 (class 1259 OID 16649)
-- Name: members; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.members (
    id smallint NOT NULL,
    member_card integer,
    locker_number integer,
    fio character varying(255) NOT NULL,
    age integer NOT NULL,
    gender integer NOT NULL,
    mobile character varying(255) NOT NULL,
    mobile2 character varying(255),
    description text,
    joining_date timestamp without time zone NOT NULL,
    parent_name character varying(255),
    parent_mobile character varying(255),
    parent_mobile2 character varying(255),
    username character varying(255) NOT NULL,
    password character varying(255),
    invite_code character varying(255),
    disabled integer
);


--
-- TOC entry 210 (class 1259 OID 16647)
-- Name: members_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.members_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2942 (class 0 OID 0)
-- Dependencies: 210
-- Name: members_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.members_id_seq OWNED BY public.members.id;


--
-- TOC entry 212 (class 1259 OID 16661)
-- Name: members_to_programs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.members_to_programs (
    id_member integer NOT NULL,
    id_program integer NOT NULL,
    joining_date timestamp without time zone NOT NULL,
    price_period character varying(256),
    price_workout_count integer,
    price_in_cents integer
);


--
-- TOC entry 209 (class 1259 OID 16633)
-- Name: programs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.programs (
    id smallint NOT NULL,
    name character varying(255) NOT NULL,
    short_name character varying(255) NOT NULL,
    description text,
    member_level character varying(255) NOT NULL,
    coach_id integer NOT NULL,
    monday_time time without time zone,
    monday_time2 time without time zone,
    tuesday_time time without time zone,
    tuesday_time2 time without time zone,
    wednesday_time time without time zone,
    wednesday_time2 time without time zone,
    thursday_time time without time zone,
    thursday_time2 time without time zone,
    friday_time time without time zone,
    friday_time2 time without time zone,
    saturday_time time without time zone,
    saturday_time2 time without time zone,
    sunday_time time without time zone,
    sunday_time2 time without time zone,
    price_in_cents integer NOT NULL,
    gym_id integer,
    monday_interval integer,
    monday_interval2 integer,
    tuesday_interval integer,
    tuesday_interval2 integer,
    wednesday_interval integer,
    wednesday_interval2 integer,
    thursday_interval integer,
    thursday_interval2 integer,
    friday_interval integer,
    friday_interval2 integer,
    saturday_interval integer,
    saturday_interval2 integer,
    sunday_interval integer,
    sunday_interval2 integer,
    start_date timestamp without time zone
);


--
-- TOC entry 208 (class 1259 OID 16631)
-- Name: programs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.programs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2943 (class 0 OID 0)
-- Dependencies: 208
-- Name: programs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.programs_id_seq OWNED BY public.programs.id;


--
-- TOC entry 220 (class 1259 OID 16745)
-- Name: users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.users (
    id integer NOT NULL,
    created_at timestamp without time zone NOT NULL,
    fio character varying(256),
    mobile character varying(256) NOT NULL,
    mobile2 character varying(256),
    username character varying(40) NOT NULL,
    password character varying(100) NOT NULL,
    invite_code character varying(100),
    group_id integer,
    who character varying(256) NOT NULL
);


--
-- TOC entry 219 (class 1259 OID 16743)
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2944 (class 0 OID 0)
-- Dependencies: 219
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- TOC entry 216 (class 1259 OID 16697)
-- Name: visits; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.visits (
    id integer NOT NULL,
    date_time timestamp without time zone NOT NULL,
    program_id integer NOT NULL,
    member_id integer NOT NULL,
    locker_number integer NOT NULL,
    who character varying(256) NOT NULL
);


--
-- TOC entry 215 (class 1259 OID 16695)
-- Name: visits_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.visits_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2945 (class 0 OID 0)
-- Dependencies: 215
-- Name: visits_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.visits_id_seq OWNED BY public.visits.id;


--
-- TOC entry 2751 (class 2604 OID 16682)
-- Name: bill id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bill ALTER COLUMN id SET DEFAULT nextval('public.bill_id_seq'::regclass);


--
-- TOC entry 2753 (class 2604 OID 16718)
-- Name: changes id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.changes ALTER COLUMN id SET DEFAULT nextval('public.changes_id_seq'::regclass);


--
-- TOC entry 2748 (class 2604 OID 16625)
-- Name: coaches id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.coaches ALTER COLUMN id SET DEFAULT nextval('public.coaches_id_seq'::regclass);


--
-- TOC entry 2746 (class 2604 OID 16589)
-- Name: gym id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gym ALTER COLUMN id SET DEFAULT nextval('public.gym_id_seq'::regclass);


--
-- TOC entry 2747 (class 2604 OID 16609)
-- Name: gym_group id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gym_group ALTER COLUMN id SET DEFAULT nextval('public.gym_group_id_seq'::regclass);


--
-- TOC entry 2750 (class 2604 OID 16652)
-- Name: members id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.members ALTER COLUMN id SET DEFAULT nextval('public.members_id_seq'::regclass);


--
-- TOC entry 2749 (class 2604 OID 16636)
-- Name: programs id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.programs ALTER COLUMN id SET DEFAULT nextval('public.programs_id_seq'::regclass);


--
-- TOC entry 2754 (class 2604 OID 16748)
-- Name: users id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- TOC entry 2752 (class 2604 OID 16700)
-- Name: visits id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.visits ALTER COLUMN id SET DEFAULT nextval('public.visits_id_seq'::regclass);


--
-- TOC entry 2924 (class 0 OID 16679)
-- Dependencies: 214
-- Data for Name: bill; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 2928 (class 0 OID 16715)
-- Dependencies: 218
-- Data for Name: changes; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.changes VALUES (2, '2020-07-28 22:33:24.674964', 1, 1, 'Hill', 'system');


--
-- TOC entry 2917 (class 0 OID 16622)
-- Dependencies: 207
-- Data for Name: coaches; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.coaches VALUES (1, 'Иванов Иван Иванович', '89211058291', NULL, NULL, 'i.ivanov', NULL, NULL);


--
-- TOC entry 2913 (class 0 OID 16586)
-- Dependencies: 203
-- Data for Name: gym; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.gym VALUES (1, 'Большая окружная', '', 30, '07:00:00', '21:00:00', 1);


--
-- TOC entry 2915 (class 0 OID 16606)
-- Dependencies: 205
-- Data for Name: gym_group; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.gym_group VALUES (1, 'Большая окружная', '', 30, 'ул. Большая окружная');


--
-- TOC entry 2921 (class 0 OID 16649)
-- Dependencies: 211
-- Data for Name: members; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.members VALUES (1, NULL, NULL, 'FIO', 10, 0, '567567567', '', '', '2020-06-01 20:20:00', '', '', '', 'username', 'password', '', NULL);


--
-- TOC entry 2922 (class 0 OID 16661)
-- Dependencies: 212
-- Data for Name: members_to_programs; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 2919 (class 0 OID 16633)
-- Dependencies: 209
-- Data for Name: programs; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.programs VALUES (1, 'Mixed Martial Arts', 'MMA', '', 'Начальный', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 200000, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-28 21:51:09.67519');


--
-- TOC entry 2930 (class 0 OID 16745)
-- Dependencies: 220
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.users VALUES (14, '2020-07-28 16:35:03.475265', 'dfgdfgdfg', '567567567', '', 'username', 'password', '', 0, 'system');
INSERT INTO public.users VALUES (13, '2020-07-28 17:07:53.470355', 'FIO', '567567567', '', 'username', 'password', '', 0, 'system');


--
-- TOC entry 2926 (class 0 OID 16697)
-- Dependencies: 216
-- Data for Name: visits; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.visits VALUES (1, '2020-07-28 22:30:12.921361', 1, 1, -1, 'system');


--
-- TOC entry 2946 (class 0 OID 0)
-- Dependencies: 213
-- Name: bill_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.bill_id_seq', 1, true);


--
-- TOC entry 2947 (class 0 OID 0)
-- Dependencies: 217
-- Name: changes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.changes_id_seq', 2, true);


--
-- TOC entry 2948 (class 0 OID 0)
-- Dependencies: 206
-- Name: coaches_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.coaches_id_seq', 1, true);


--
-- TOC entry 2949 (class 0 OID 0)
-- Dependencies: 204
-- Name: gym_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.gym_group_id_seq', 1, true);


--
-- TOC entry 2950 (class 0 OID 0)
-- Dependencies: 202
-- Name: gym_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.gym_id_seq', 1, true);


--
-- TOC entry 2951 (class 0 OID 0)
-- Dependencies: 210
-- Name: members_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.members_id_seq', 1, true);


--
-- TOC entry 2952 (class 0 OID 0)
-- Dependencies: 208
-- Name: programs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.programs_id_seq', 1, true);


--
-- TOC entry 2953 (class 0 OID 0)
-- Dependencies: 219
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.users_id_seq', 14, true);


--
-- TOC entry 2954 (class 0 OID 0)
-- Dependencies: 215
-- Name: visits_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.visits_id_seq', 1, true);


--
-- TOC entry 2768 (class 2606 OID 16684)
-- Name: bill bill_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bill
    ADD CONSTRAINT bill_key PRIMARY KEY (id);


--
-- TOC entry 2772 (class 2606 OID 16723)
-- Name: changes changes_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.changes
    ADD CONSTRAINT changes_key PRIMARY KEY (id);


--
-- TOC entry 2756 (class 2606 OID 16594)
-- Name: gym claims_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gym
    ADD CONSTRAINT claims_pkey PRIMARY KEY (id);


--
-- TOC entry 2760 (class 2606 OID 16630)
-- Name: coaches coaches_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.coaches
    ADD CONSTRAINT coaches_key PRIMARY KEY (id);


--
-- TOC entry 2758 (class 2606 OID 16614)
-- Name: gym_group gym_group_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gym_group
    ADD CONSTRAINT gym_group_key PRIMARY KEY (id);


--
-- TOC entry 2766 (class 2606 OID 16665)
-- Name: members_to_programs members2programs_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.members_to_programs
    ADD CONSTRAINT members2programs_key PRIMARY KEY (id_member, id_program);


--
-- TOC entry 2764 (class 2606 OID 16657)
-- Name: members members_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.members
    ADD CONSTRAINT members_key PRIMARY KEY (id);


--
-- TOC entry 2762 (class 2606 OID 16641)
-- Name: programs programs_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.programs
    ADD CONSTRAINT programs_key PRIMARY KEY (id);


--
-- TOC entry 2774 (class 2606 OID 16753)
-- Name: users users_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_key PRIMARY KEY (id);


--
-- TOC entry 2770 (class 2606 OID 16702)
-- Name: visits visits_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.visits
    ADD CONSTRAINT visits_key PRIMARY KEY (id);


--
-- TOC entry 2780 (class 2606 OID 16685)
-- Name: bill bill_fk1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bill
    ADD CONSTRAINT bill_fk1 FOREIGN KEY (member_id) REFERENCES public.members(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2781 (class 2606 OID 16690)
-- Name: bill bill_fk2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bill
    ADD CONSTRAINT bill_fk2 FOREIGN KEY (program_id) REFERENCES public.programs(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2784 (class 2606 OID 16724)
-- Name: changes changes_fk1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.changes
    ADD CONSTRAINT changes_fk1 FOREIGN KEY (coach_id) REFERENCES public.coaches(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2785 (class 2606 OID 16729)
-- Name: changes changes_fk2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.changes
    ADD CONSTRAINT changes_fk2 FOREIGN KEY (program_id) REFERENCES public.programs(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2775 (class 2606 OID 16615)
-- Name: gym gym_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.gym
    ADD CONSTRAINT gym_fk FOREIGN KEY (gorup_id) REFERENCES public.gym_group(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2778 (class 2606 OID 16666)
-- Name: members_to_programs members_fk1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.members_to_programs
    ADD CONSTRAINT members_fk1 FOREIGN KEY (id_member) REFERENCES public.members(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2779 (class 2606 OID 16671)
-- Name: members_to_programs members_fk2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.members_to_programs
    ADD CONSTRAINT members_fk2 FOREIGN KEY (id_program) REFERENCES public.programs(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2776 (class 2606 OID 16642)
-- Name: programs programs_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.programs
    ADD CONSTRAINT programs_fk FOREIGN KEY (coach_id) REFERENCES public.coaches(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2777 (class 2606 OID 16759)
-- Name: programs programs_gym_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.programs
    ADD CONSTRAINT programs_gym_fk FOREIGN KEY (gym_id) REFERENCES public.gym(id) ON DELETE RESTRICT;


--
-- TOC entry 2782 (class 2606 OID 16703)
-- Name: visits visits_fk1; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.visits
    ADD CONSTRAINT visits_fk1 FOREIGN KEY (member_id) REFERENCES public.members(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2783 (class 2606 OID 16708)
-- Name: visits visits_fk2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.visits
    ADD CONSTRAINT visits_fk2 FOREIGN KEY (program_id) REFERENCES public.programs(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


-- Completed on 2020-07-28 23:22:50

--
-- PostgreSQL database dump complete
--

